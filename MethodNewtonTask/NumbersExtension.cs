﻿using System;

namespace MethodNewtonTask
{
    public static class NumbersExtension
    {
        /// <summary>
        /// Initializes static members of the <see cref="NumbersExtension"/> class.
        /// </summary>
        public static readonly AppSettings AppSettings = new AppSettings { Epsilon = double.Epsilon };

        /// <summary>
        /// Find n-th root of number with the given accuracy.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <param name="degree">Root degree.</param>
        /// <param name="accuracy">Accuracy value.</param>
        /// <returns> n-th root of number.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown when number is negative and n degree is even.
        /// -or- 
        /// degree is less than zero
        /// -or-
        /// number is NaN, double.NegativeInfinity, double.PositiveInfinity.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when accuracy is less than zero.
        /// -or- 
        /// accuracy is more than `Epsilon`.
        /// </exception>
        public static double FindNthRoot(double number, int degree, double accuracy)
        {
            if (number < 0 && degree % 2 == 0)
            {
                throw new ArgumentException("number is negative and n degree is even.");
            }

            if (degree < 0)
            {
                throw new ArgumentException("degree is less than zero.");
            }

            if (double.IsNaN(number) || double.IsNegativeInfinity(number) || double.IsPositiveInfinity(number))
            {
                throw new ArgumentException("number is NaN or double.NegativeInfinity or double.PositiveInfinity");
            }

            if (accuracy < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(accuracy), "accuracy is less than zero.");
            }

            if (accuracy > AppSettings.Epsilon)
            {
                throw new ArgumentOutOfRangeException(nameof(accuracy), "accuracy is more than Eplison.");
            }

            double differenceOfNumbers = accuracy + 1;
            double startRoot = 0.5;
            double nextRoot = 0;

            while (differenceOfNumbers > accuracy)
            {
                nextRoot = ((degree - 1) * startRoot / degree) + (number / (degree * Math.Pow(startRoot, degree - 1)));
                differenceOfNumbers = Math.Abs(nextRoot - startRoot);
                startRoot = nextRoot;
            }

            return nextRoot;
        }
    }
}
